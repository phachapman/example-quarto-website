#!/bin/bash

# Repository README is rendered from README.q/rmd.
# Compare rendered README.md and committed README.md in CI pipelines.
# cmp is available from BusyBox in Alpine.
# Run 'quarto render README.qmd -o README-render.md' to test script locally.

path_rendered_readme=./README-render.md

# Use -s to suppress comparison output; evaluates to true where equivalent. 
if cmp ./README.md $path_rendered_readme -s
then
  echo -e "Rendered README matches repository README (./README.md); repository README is up to date."
  exit 0
else
  echo -e "README.md is out of date or could not be rendered; please re-render from README.q/rmd."
  exit 1
fi